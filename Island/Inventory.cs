﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Inventory : MonoBehaviour {
	private static Inventory instance;
	public List<string> items;

	public Inventory() {
		instance = this;
	}

	public static Inventory getInstance() {
		if(instance == null) {
			instance = new Inventory ();
		}
		return instance;
	}

	// Use this for initialization
	void Start () {
		this.items = new List<string> (6) {"", "", "", "", "", ""};
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void addItem (string item_name) {
		if (this.spaceAvaiable ()) {
			int avaiable = this.items.FindIndex (x => x == "");
			this.items [avaiable] = item_name;
		}
	}

	public void removeItem (string item_name) {
		if(this.hasItem(item_name)) {
			int index = this.items.FindIndex (x => x == item_name);
			this.items [index] = "";
		}
	}

	public string getItemName (int slot_number) {
			return this.items[slot_number];
	}

	public void emptySlot(int slot_number) {
		this.items [slot_number] = "";
	}

	public bool spaceAvaiable () {
		List<string> items = this.items.FindAll (x => x.Length > 0);
		return (items.Count < 6);
	}

	public bool hasItem(string item_name) {
		return this.items.Contains (item_name);
	}


}
