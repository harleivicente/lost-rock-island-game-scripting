﻿using UnityEngine;
using System.Collections;

/*

	Decides if player is looking at action/item and
	it is possible to perform them.

*/
public class PlayerVision : MonoBehaviour {
	private static PlayerVision instance;
	private Camera player_camera;
	private GameObject player;
	private GameControl gameControl;

	// If looking at inventory item
	public bool looking_at_item = false;
	public string item_type;
	public int item_code;
	public GameObject item_obj;

	// If action is avaiable and may be performed
	public bool action_avaiable = false;
	public string action_name = "";
	public string action_text = "";

	public PlayerVision () {
		instance = this;
	}

	public static PlayerVision getInstance() {
		if (instance == null) {
			instance = new PlayerVision();
		}
		return instance;
	}

	void Start () {
		gameControl = GameControl.getInstance ();
		player = GameObject.Find("Player");
		player_camera = GetComponent<Camera> ();
	}
	
	void Update () {

		// Check if ui is enabled
		if (gameControl.ui_activated) {
			checkLookingItemOrAction ();
		} else {
			action_avaiable = false;
		}
	}


	/*
		Updates:
			looking_at_item
			item_type
			item_code
			item_obj
			action_avaiable
			action_name
			action_text

	*/
	private void checkLookingItemOrAction() {
		Ray ray = player_camera.ViewportPointToRay (new Vector3 (0.5F, 0.5F, 0));
		RaycastHit hit;
		Physics.Raycast (ray, out hit);

		// Check if looking at anything
		if (hit.transform != null) {

			GameObject item = hit.transform.gameObject;
			Item item_component = item.GetComponent<Item> ();
			float distance = Vector3.Distance (item.transform.position, player.transform.position);

			// Check if it's an item and is not too far
			if (item_component != null && distance <= 1.5f && item.GetComponent<MeshRenderer>().enabled) {
				looking_at_item = true;
				this.item_obj = item;
				item_type = item_component.item_type;
				item_code = item_component.item_code;
				this.action_avaiable = true;
				this.action_name = "pick_up";
			} else {
				Action action_component = item.GetComponent<Action> ();

				if (action_component != null && distance <= 3f && action_component.enabled) {
					this.action_avaiable = true;
					this.action_name = action_component.action_name;
					this.action_text = action_component.action_text;
					this.looking_at_item = false;
				} else {
					looking_at_item = false;
					this.action_avaiable = false;			
				}
			}

		}

	}
}

