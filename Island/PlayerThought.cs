﻿using UnityEngine;
using System.Collections;

/*
	If player walks into collider will trigger one message.

*/
public class PlayerThought : MonoBehaviour {
	private UiControl uiControl;

	public int chance; // From 0 to 100
	public string[] messages;
	public bool loopable;
	public bool only_play_if_imediate = false;

	// Use this for initialization
	void Start () {
		if (chance < 0)
			chance = 0;
		else if (chance > 100)
			chance = 100;

		uiControl = UiControl.getInstance ();
	}

	void OnTriggerEnter (Collider col)
	{
		if (col.name == "Player") {
			int random_number = Random.Range (1, 100);
			if (random_number <= chance && messages.Length > 0) {

				if (!only_play_if_imediate || !uiControl.showing_text) {
					int index = Random.Range (0, messages.Length);
					uiControl.addStoryBit (messages[index]);

					if (!loopable) {
						Destroy (this.gameObject);
					}
				}

			}
		}

	}
}
