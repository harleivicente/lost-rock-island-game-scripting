﻿using UnityEngine;
using System.Collections;

/*
	Control execution of actions


*/
public class ActionControl : MonoBehaviour {
	private PlayerVision playerVision;
	private Inventory inventory;
	private UiControl uiControl;
	private GameObject flyingShard;
	private GameObject pryFlyingShard;
	private GameControl gameControl;

	// World status
	public bool windmillFixed = false;
	public int embededShards = 0;

	// Use this for initialization
	void Start () {
		this.gameControl = GameControl.getInstance ();
		this.uiControl = UiControl.getInstance ();
		this.playerVision = PlayerVision.getInstance ();
		this.inventory = Inventory.getInstance ();
	}
	
	// Update is called once per frame
	void Update () {
		checkIfActionIsBeingTriggered ();
	}

	// Checks if action is being executed
	private void checkIfActionIsBeingTriggered() {
		if (this.playerVision.action_avaiable && Input.GetKeyDown ("e") && !this.uiControl.showing_text) {
			switch (this.playerVision.action_name) {
			case "pick_up":
				actionPickUp ();
				break;
			case "open_gate":
				actionOpenGate ();
				break;
			case "remove_blockage":
				actionRemoveBlockage ();
				break;

			case "repair_bridge":
				actionRepairBridge ();
				break;

			case "open_bridge":
				actionOpenBridge ();
				break;

			case "dig":
				actionDig ();
				break;

			case "repair_well":
				actionRepairWell ();
				break;

			case "use_well":
				actionUseWell ();
				break;

			case "pry":
				actionPry ();
				break;

			case "repair_windmill":
				actionRepairWindmill();
				break;

			case "mill":
				actionMill ();
				break;

			case "fill":
				actionFeed ();
				break;

			case "power":
				actionPower ();
				break;

			default:
				break;
			}	
		}
	}

	// Picks up item in front of player and puts it in inventory
	private void actionPickUp() {
		string item_name = playerVision.item_type;

		// remove item from game
		if (inventory.spaceAvaiable ()) {
			playerVision.item_obj.SetActive (false);
			inventory.addItem (item_name);

			if (!gameControl.shard_picked && item_name == "shard") {
				gameControl.shard_picked = true;
				uiControl.addStoryBit ("This rock seems to have the same energy as the earth crystal. It could be what is needs...");
			}
		} else {
			uiControl.addStoryBit ("I can’t carry anything else. I should leave some stuff behind. [USE 1-6 TO DROP ITEMS.]");
		}

	}

	private void actionPower() {
		bool shard = inventory.hasItem ("shard");

		if (shard) {
			inventory.removeItem ("shard");
			this.embededShards++;

			// Place shard
			int shard_to_activate = this.embededShards;
			GameObject.Find ("placed_shard_" + shard_to_activate.ToString ()).GetComponent<MeshRenderer> ().enabled = true;
			gameControl.shards_placed++;

			if (gameControl.shards_placed < 7) {
				uiControl.addStoryBit ("Got another crystal in place. I still need more...");
			} else {

				gameControl.ui_activated = false;
				GameObject.Find ("Crystal core").GetComponent<Animator> ().speed = 4;
				GameObject.Find ("Crystal core").GetComponent<AudioSource> ().volume = 1;
				Invoke ("delayed_power_up_events", 5f);
				
			}

		} else {
			uiControl.addStoryBit ("I should find whatever was embeded in these slots.");
		}
	}

	private void delayed_power_up_events() {
		uiControl.flashScreenEndGame ();
	}

	private void actionMill() {

		if (this.windmillFixed) {
			bool grains = inventory.hasItem ("whole_grain");

			if (grains) {
				inventory.removeItem ("whole_grain");
				inventory.addItem ("ground_grain");
				uiControl.addStoryBit ("Well, I have got processed grains now... they look a little moldy though. I wonder if they are good to eat.");

			} else {
				uiControl.addStoryBit ("I guess I fixed it... I could use it if I wanted to.");
			}
		} else {
			uiControl.addStoryBit ("Whoever lives here must use this to grind grains.... but it's not getting power.");
		}
	}

	private void actionFeed() {
		bool ground = inventory.hasItem ("ground_grain");
		bool whole = inventory.hasItem ("whole_grain");
		GameObject tray = GameObject.Find ("Food_tray");
		Action action = tray.GetComponent<Action> ();
		Animator animator = tray.GetComponent<Animator> ();

		if (ground) {
			inventory.removeItem ("ground_grain");
			animator.SetBool ("filled", true);
			action.enabled = false;

			uiControl.addStoryBit ("In all honesty, this food does not look very good.");
			gameControl.animal_fed = true;

		} else if (whole) {
			uiControl.addStoryBit ("The GOAT looks hungry but I don't think it will eat whole grains...i'd need to break them down first.");
		} else {
			uiControl.addStoryBit ("No food here...");
		}

	}

	private void actionPry() {

		// Check if needed items are avaiable
		bool item = inventory.hasItem("pickaxe");

		if (item) {
			GameObject rock = GameObject.Find ("rock_top_shard");
			Action action = rock.GetComponent<Action> ();
			GameObject.Find ("Shard_4").SetActive (false);

			Vector3 location = new Vector3 (-53.91f, 66.282f, -108.144f);
			Vector3 force = new Vector3 (-200, 20, 0);
			Vector3 torque = new Vector3 (-4, 22, 3);
			this.pryFlyingShard = Instantiate (Resources.Load ("Prefabs/shard"), location, Quaternion.identity) as GameObject;
			this.pryFlyingShard.GetComponent<Rigidbody> ().detectCollisions = false;
			this.pryFlyingShard.GetComponent<Rigidbody> ().AddTorque (torque);
			this.pryFlyingShard.GetComponent<Rigidbody> ().AddForce (force);
			Invoke ("fixRockRigidBody", 0.2f);
			action.enabled = false;
			uiControl.addStoryBit ("Nice... didn't even damage the shard.");


		} else {
			uiControl.addStoryBit ("This looks like one of the shards I need. Don't think I can remove it with my bare hands.");
		}
	}

	private void actionOpenGate() {

		// Check if needed items are avaiable
		bool item = inventory.hasItem("keys");

		if (item) {
			GameObject gate = GameObject.Find ("Gate");
			Animator gate_animator = gate.GetComponent<Animator> ();
			gate_animator.SetBool ("gate_unlocked", true);
			gate.GetComponent<BoxCollider> ().enabled = false;
			GameObject.Find ("open_gate_blocker_a").GetComponent<BoxCollider>().enabled = true;
			GameObject.Find ("open_gate_blocker_b").GetComponent<BoxCollider>().enabled = true;
			gate.GetComponent<Action> ().enabled = false;
			uiControl.addStoryBit ("Ok. Let's see what I can find up there.");
		} else {
			uiControl.addStoryBit ("...I might need a key to open this.");
		}
	}

	private void actionRepairWindmill() {

		// Check if needed items are avaiable
		bool belt = inventory.hasItem("belt");
		bool cog = inventory.hasItem("cog");
		bool hammer = inventory.hasItem("hammer");

		if (belt && cog && hammer) {
			GameObject windmillAction = GameObject.Find ("ActionRepairWindmill");
			Action actionAction = windmillAction.GetComponent<Action> ();
			GameObject windmill = GameObject.Find ("Windmill");
			GameObject broken_parts = GameObject.Find ("Windmill_broken_parts");
			Animator animator = windmill.GetComponent<Animator>();

			inventory.removeItem ("belt");
			inventory.removeItem ("cog");
			animator.SetBool ("fixed", true);
			broken_parts.SetActive (false);
			actionAction.enabled = false;
			this.windmillFixed = true;
			uiControl.addStoryBit ("The sails are rotating... the mill should be getting power now.");

		} else {
			uiControl.addStoryBit ("The main axis is broken... can't use the mill unless I fix this.");
		}
	}

	private void actionRepairWell() {

		// Check if needed items are avaiable
		bool rope = inventory.hasItem("rope");
		bool bucket = inventory.hasItem("bucket");

		if (rope && bucket) {
			GameObject well = GameObject.Find ("WellM");
			Animator animator = well.GetComponent<Animator> ();
			Action action = well.GetComponent<Action> ();

			inventory.removeItem ("rope");
			inventory.removeItem ("bucket");
			animator.SetBool ("well_repaired", true);
			action.action_name = "use_well";
			action.action_text = "User well";

			uiControl.addStoryBit ("It should work now.");
		} else {
			uiControl.addStoryBit ("I think I see something at the bottom of this well. Maybe if I fix it I could get whatever it is.");
		}
	}

	private void actionUseWell() {
		GameObject well = GameObject.Find ("WellM");
		Action action = well.GetComponent<Action> ();

		if (action.enabled) {
			Vector3 location = new Vector3 (-51.354f, 55.5f, -97.095f);
			Vector3 force = new Vector3 (0, 300, 50);
			Vector3 torque = new Vector3 (1, 2, 3);
			this.flyingShard = Instantiate (Resources.Load ("Prefabs/shard"), location, Quaternion.identity) as GameObject;
			this.flyingShard.GetComponent<Rigidbody> ().detectCollisions = false;
			this.flyingShard.GetComponent<Rigidbody> ().AddTorque (torque);
			this.flyingShard.GetComponent<Rigidbody> ().AddForce (force);
			Invoke ("fixWellRigidBody", 1f);

			action.enabled = false;

		}
	}

	public void fixWellRigidBody() {
		this.flyingShard.GetComponent<Rigidbody> ().detectCollisions = true;
	}

	public void fixRockRigidBody() {
		this.pryFlyingShard.GetComponent<Rigidbody> ().detectCollisions = true;
	}

	private void actionRemoveBlockage() {

		// Check if needed items are avaiable
		bool item = inventory.hasItem("pickaxe");

		if (item) {
			GameObject blockage = GameObject.Find ("blockage");
			GameObject colider = GameObject.Find ("blockage_colider");
			blockage.SetActive (false);
			colider.GetComponent<BoxCollider> ().enabled = false;
		} else {
			uiControl.addStoryBit ("These rocks are too heavy to remove manually.");
		}
	}

	private void actionDig() {

		// Check if needed items are avaiable
		bool item = inventory.hasItem("shovel");

		if (item) {
			GameObject ground = GameObject.Find ("ground_a");
			ground.SetActive (false);
			uiControl.addStoryBit ("It looks like someone hid a shard here.");

		} else {
			uiControl.addStoryBit ("The dirt around this area seems to have been moved around recently. I could dig it up...");
		}
	}

	private void actionRepairBridge() {

		// Check if needed items are avaiable
		bool bracket = inventory.hasItem("bracket");
		bool nails = inventory.hasItem("nails");
		bool plank = inventory.hasItem("plank");
		bool rope_guide = inventory.hasItem("rope_guide");
		bool lever = inventory.hasItem("lever");
		bool hammer = inventory.hasItem("hammer");

		if (bracket && nails && plank && rope_guide && lever && hammer) {
			GameObject bridge = GameObject.Find ("Bridge");
			Animator animator = bridge.GetComponent<Animator> ();
			Action action = bridge.GetComponent<Action> ();

			inventory.removeItem ("bracket");
			inventory.removeItem ("nails");
			inventory.removeItem ("rope_guide");
			inventory.removeItem ("plank");
			inventory.removeItem ("lever");

			animator.SetBool ("bridge_fixed", true);

			action.action_name = "open_bridge";
			action.action_text = "Open bridge.";
			

			uiControl.addStoryBit ("There we go !. It should work now.");
		} else {
			uiControl.addStoryBit ("This bridge has been damaged. If I find the right parts I might be able to repair it.");
		}
	}

	private void actionOpenBridge() {
			GameObject bridge = GameObject.Find ("Bridge");
			BoxCollider colider = bridge.GetComponent<BoxCollider> ();
			Animator animator = bridge.GetComponent<Animator> ();
			Action action = bridge.GetComponent<Action> ();

			animator.SetBool ("bridge_opened", true);
			colider.enabled = false;
			uiControl.addStoryBit ("Well... let's find out what's on the other side.");
		action.enabled = false;
	}

}
