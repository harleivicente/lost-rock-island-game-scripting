﻿using UnityEngine;
using System.Collections;

/*
	Controls game flow and story telling.
	
*/
public class GameControl : MonoBehaviour {
	private static GameControl instance;
	private UiControl uiControl;

	// Game states

	/*
		Has the inventory, crosshair, tooltips and actions been enabled ?
	*/
	public bool ui_activated = false;
	public bool shard_picked = false;
	public int waiting_for_confirm_before_interaction = 0;
	public bool animal_fed = false;
	public int shards_placed = 0;

	public GameControl() {
		instance = this;
	}

	public static GameControl getInstance() {
		if (instance == null)
			instance = new GameControl ();
		return instance;
	}

	// Use this for initialization
	void Start () {
		uiControl = UiControl.getInstance ();

		// Start game story
		Invoke ("event_game_started", 2f);
	}
	
	// Update is called once per frame
	void Update () {
	
		if (waiting_for_confirm_before_interaction == 2)
			game_interaction_start ();
	}

	/*
		Trigger the function: event_TYPE

	*/
	public void executeStoryEvent(string type) {
		Invoke ("event_" + type, 0f);
	}

	private void event_crystal_core_found(){
		GameObject.Find ("crystal core slots").GetComponent<StoryTriggerView> ().enabled = true;
		GameObject.Find ("Flying machine").GetComponent<StoryTriggerView> ().enabled = true;
	}

	private void event_crystal_core_slots_found(){
		GameObject.Find ("crystal core slots").GetComponent<CapsuleCollider> ().enabled = false;
		waiting_for_confirm_before_interaction = 1;
	}


	private void game_interaction_start() {
		ui_activated = true;
		//uiControl.addStoryBit ("FIND WHAT THE CRYSTAL IS MISSING.");
		waiting_for_confirm_before_interaction = 3;
	}


	private void event_game_started() {
		uiControl.addStoryBit ("Not the safest landing. My flying machine was completely destroyed… I think I may be stuck here. Still, I should go look for the island’s source of power.");
	}



}


