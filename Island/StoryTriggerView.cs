﻿using UnityEngine;
using System.Collections;

public class StoryTriggerView : MonoBehaviour {
	private GameObject player;
	private Camera game_camera;
	private UiControl uiControl;
	private GameControl gameControl;
	private float last_time_used = -1f;

	public float range;
	public bool only_if_instant = true;
	public int cooldown = 120; // seconds | -1 means no cd
	public string trigger_event = "";
	public string[] thoughts;
	public bool one_time_only = true;

	// Use this for initialization
	void Start () {
		gameControl = GameControl.getInstance ();
		uiControl = UiControl.getInstance ();
		player = GameObject.Find ("Player");
		game_camera = GameObject.Find("FirstPersonCharacter").GetComponent<Camera> ();
	}
	
	// Update is called once per frame
	void Update () {
		float distance = Vector3.Distance (this.transform.position, player.transform.position);
		if (distance <= range) {
			Ray ray = game_camera.ViewportPointToRay (new Vector3(.5f, .5f, 0f));
			RaycastHit hit;
			Physics.Raycast (ray, out hit);

			// Looking at item and within range
			if (hit.transform != null && hit.transform.name == this.name){
				
				// Check if needs to be instant
				if (!only_if_instant || !uiControl.showing_text) {

					// Check for cooldown
					if(cooldown < 0 || last_time_used < 0 || (Time.fixedTime - last_time_used) >= cooldown){

						// Proceed with thoughts
						int random_index = Random.Range(0, thoughts.Length);
						uiControl.addStoryBit (thoughts[random_index]);

						// Trigger story event
						if(trigger_event != null && trigger_event.Length > 0)
							gameControl.executeStoryEvent(trigger_event);

						// Update time
						last_time_used = Time.fixedTime;

						// Disable behavior if one time only
						if (one_time_only)
							this.gameObject.GetComponent<StoryTriggerView> ().enabled = false;
					}

				}

			};
		}
	}


}
