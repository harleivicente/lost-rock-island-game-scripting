﻿using UnityEngine;
using System.Collections;

public class Goat : MonoBehaviour {
	private NavMeshAgent agent;
	private GameObject goat;
	private GameObject player;
	private GameControl gameControl;
	private AudioSource audioScr;
	private Animator animator;
	private AudioClip clip_scream;
	private AudioClip clip_cough;
	public float range;
	private bool vomit_shard = false;
	private bool goat_interrupted = false;
	private Vector3[] destinations = new Vector3[2];
	private float max_time_eating = 40;
	private float time_spent_eating = 0;
	private float max_time_throwing_up = 15;
	private float time_throwing_up = 0;
	private int next_destination = -1;
	/*
		0 waiting for food
		1 heading for food
		2 eating
		3 waiting for player to approach
		4 throwing up
		5 rest
	*/
	public int goat_status = 0;


	// Use this for initialization
	void Start () {
		gameControl = GameControl.getInstance ();
		player = GameObject.Find ("Player");
		goat = GameObject.Find ("Goat");
		animator = goat.GetComponent<Animator> ();
		audioScr = goat.GetComponent<AudioSource> ();
		agent = gameObject.GetComponent<NavMeshAgent> ();
		clip_scream = Resources.Load("Audio/scream") as AudioClip;
		clip_cough = Resources.Load("Audio/cough") as AudioClip;
		agent.updatePosition = true;
		agent.updateRotation = true;

		destinations [0] = GameObject.Find ("flag_a").transform.position;
		destinations [1] = GameObject.Find ("flag_b").transform.position;

	}
	
	// Update is called once per frame
	void Update () {
		float distance_from_player = Vector3.Distance (player.transform.position, transform.position);

		// 0 => 1
		if (gameControl.animal_fed && goat_status == 0) {
			// distance from flag_c
			float distance_from_midpoint = Vector3.Distance(GameObject.Find ("flag_c").transform.position, transform.position);

			if(distance_from_midpoint <= 2f)
				goat_status = 1;
		}

		// STATUS 0
		if(goat_status == 0){

			if (agent.remainingDistance <= 4f) {
				next_destination++;

				if (next_destination >= destinations.Length)
					next_destination = 0;

				agent.destination = destinations [next_destination];
			}

			if (distance_from_player <= range) {

				if (!goat_interrupted) {
					agent.Stop ();
					animator.SetBool ("walking", false);
					audioScr.clip = clip_scream;
					audioScr.loop = true;
					audioScr.time = 0;
					audioScr.Play ();
					goat_interrupted = true;
				}

			} else {
				if (goat_interrupted) {
					agent.Resume ();
					animator.SetBool ("walking", true);
					audioScr.Stop ();
					goat_interrupted = false;
				}
			}

		}


		// STATUS 1
		if (goat_status == 1) {
			agent.destination = new Vector3(-45.36f, 55.02f, -95.59f);

			// When arriving
			if (agent.remainingDistance < .2f) {
				agent.Stop ();
				animator.SetBool ("walking", false);
				animator.SetBool ("eating", true);
				goat_status = 2;
			} else {
			
				// stop if player approaches
				if (distance_from_player <= range) {

					if (!goat_interrupted) {
						agent.Stop ();
						animator.SetBool ("walking", false);
						audioScr.clip = clip_scream;
						audioScr.loop = true;
						audioScr.time = 0;
						audioScr.Play ();
						goat_interrupted = true;
					}

				} else {
					if (goat_interrupted) {
						agent.Resume ();
						animator.SetBool ("walking", true);
						audioScr.Stop ();
						goat_interrupted = false;
					}
				}
			
			}
		}


		// STATUS 2
		if(goat_status == 2) {
			time_spent_eating += Time.deltaTime;
			if (time_spent_eating >= max_time_eating) {
				animator.SetBool ("eating", false);

				audioScr.clip = clip_scream;
				audioScr.loop = true;
				audioScr.time = 0;
				audioScr.Play ();

				// Remove food
				GameObject.Find("Food_tray").GetComponent<Animator>().SetBool("filled", false);

				goat_status = 3;
			}
		}

		// STATUS 3
		if(goat_status == 3){
			// if player aproaches
			if(distance_from_player <= 6){
				animator.SetBool ("vomiting", true);

				audioScr.clip = clip_cough;
				audioScr.loop = false;
				audioScr.time = 0;
				audioScr.Play ();

				goat_status = 4;	
			}
		}

		// STATUS 4
		if(goat_status == 4){
			time_throwing_up += Time.deltaTime;

			// 5.23 seconds in, launch shard
			if(time_throwing_up >= 5.8f && !vomit_shard) {
				GameObject shard = GameObject.Find ("goat_shard");
				shard.GetComponent<MeshRenderer> ().enabled = true;
				shard.GetComponent<Rigidbody> ().AddForce(new Vector3(0f, -10f, 0f));	
				vomit_shard = true;
			}


			// stop vomiting
			if (time_throwing_up >= max_time_throwing_up) {
				animator.SetBool ("vomiting", false);

				// Idle audio
				audioScr.clip = clip_scream;
				audioScr.loop = true;
				audioScr.time = 0;
				audioScr.Play ();

				goat_status = 5;
			}
		}

		// STATUS 5
		if(goat_status == 5){
			
		}

	}
}
