﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/*
	Controls the ui of the island scene.

		- flashing screen at end of game
		- displaying story
		- displaying tooltips
		- displaying game feedback
		- display of inventory icons
		- displaying overlay
		- droping inventory items

*/
public class UiControl : MonoBehaviour {
	private GameObject player;
	private static UiControl instance;
	private List<string> story_bits = new List<string> ();
	private GameControl gameControl;
	private bool flashing = false;
	private PlayerVision playerVision;

	private GameObject text_small;
	private GameObject text_large;
	private GameObject text_small_t;
	private GameObject text_large_t;
	private GameObject flash;
	private GameObject feedback;
	private GameObject feedback_t;

	// Control of character text
	public bool waiting_for_dismiss = false;
	public bool showing_text = false; // If a player thought is being displayed
	private string current_text = ""; // Current player thought being displayed
	private bool text_needed = false; // Whether or not a player thought is avaiable to be displayed

	// Control tooltip text
	private bool tooltip_needed = false;
	private string tooltip_text = "";

	// Inventory slots
	private GameObject[] inventory_slots;
	private Inventory inventory;

	public UiControl() {
		instance = this;
	}

	public static UiControl getInstance() {
		if (instance == null)
			instance = new UiControl ();
		return instance;
	}
		
	// Use this for initialization
	void Start () {
		playerVision = PlayerVision.getInstance ();
		gameControl = GameControl.getInstance ();
		player = GameObject.Find ("Player");

		flash = GameObject.Find ("Flash");
		feedback = GameObject.Find ("feedback");
		feedback_t = GameObject.Find ("feedback_t");
		text_large = GameObject.Find ("text_large");
		text_large_t = GameObject.Find ("text_large_t");
		text_small = GameObject.Find ("text_small");
		text_small_t = GameObject.Find ("text_small_t");

		// Fill out inventory items
		inventory_slots = new GameObject[6];
		inventory = Inventory.getInstance ();
		for(int i = 0; i < 6; i++) {
			this.inventory_slots [i] = GameObject.Find ("slot_" + i.ToString());
		}
	}

	void Update ()
	{
		checkIfTooptipNeeded ();
		animateFlashOverlay ();
		showStoryBits ();
		showTooltipText ();

		updateOverlay ();

		checkIfDropingItem ();

		// Check if story is being dismissed
		if(showing_text && Input.GetKeyDown("e") && waiting_for_dismiss){
			waiting_for_dismiss = false;

			if (gameControl.waiting_for_confirm_before_interaction == 1)
				gameControl.waiting_for_confirm_before_interaction = 2;
		}

	}

	// Check if player is trying to drop items
	private void checkIfDropingItem () {
		if (Input.GetKeyDown ("1"))
			this.dropInventoryItem (0);
		if(Input.GetKeyDown("2"))
			this.dropInventoryItem (1);
		if(Input.GetKeyDown("3"))
			this.dropInventoryItem (2);
		if(Input.GetKeyDown("4"))
			this.dropInventoryItem (3);
		if(Input.GetKeyDown("5"))
			this.dropInventoryItem (4);
		if(Input.GetKeyDown("6"))
			this.dropInventoryItem (5);
	}

	private void updateOverlay() {
		Image crosshair = GameObject.Find ("crosshair").GetComponent<Image> ();
		Image inventory = GameObject.Find ("inventory").GetComponent<Image> ();

		if (gameControl.ui_activated) {
			crosshair.enabled = true;
			inventory.enabled = true;
		} else {
			crosshair.enabled = false;
			inventory.enabled = false;
		}
	
		updateInventoryIcons ();
	}

	// Displays the proper inventory items on the screen
	private void updateInventoryIcons() {
		for (int i = 0; i < 6; i++) {
			string item = inventory.items [i];
			GameObject slot_obj = this.inventory_slots [i];
			CanvasGroup slot_canvas_group = slot_obj.GetComponent<CanvasGroup> ();
			Image slot_image = slot_obj.GetComponent<Image> ();

			if (item.Length > 0 && gameControl.ui_activated) {
				slot_canvas_group.alpha = 1;
				slot_image.sprite = Resources.Load("Images/inventory_icons/" + item, typeof(Sprite)) as Sprite;
			} else {
				slot_canvas_group.alpha = 0;
			}
		}
	}

	// Puts another story bit in line for display
	public void addStoryBit(string story) {
		story_bits.Add (story);
		text_needed = true;
	}

	// Initiates the screen flash animation and ends game.
	public void flashScreenEndGame ()
	{
		flash.GetComponent<CanvasGroup>().alpha = 0;
		flashing = true;
	}

	// Removes an item from inventory and drops it where player stands
	public void dropInventoryItem(int slot) {
		string item = this.inventory.getItemName (slot);
		this.inventory.emptySlot (slot);
		if (item.Length > 0) {
			GameObject drop = Instantiate(Resources.Load("Prefabs/" + item), this.player.transform.position, Quaternion.identity) as GameObject;
			GameObject camera = GameObject.Find ("FirstPersonCharacter");
			Vector3 force = 300 * Vector3.Normalize (camera.transform.forward);
			drop.GetComponent<Rigidbody> ().AddTorque (new Vector3(1,1,0));
			drop.GetComponent<Rigidbody> ().AddForce(force);
		}
			
	}

	// Controls flash animation once triggered
	private void animateFlashOverlay() {
		if (this.flashing)
		{
			CanvasGroup flashCG = this.flash.GetComponent<CanvasGroup> ();
			flashCG.alpha = flashCG.alpha + (Time.deltaTime / 1);
			if (flashCG.alpha >= 1)
			{
				flashing = false;

				// Loads the new level
				SceneManager.LoadScene("story_end");

			}
		}
	}

	/**
		Check if a tooltip needs to be displayed and what text it should be.

		PlayerVision.action_avaiable defines if an action could be performed.

	**/
	private void checkIfTooptipNeeded() {
		if (playerVision.looking_at_item) {
			this.tooltip_needed = true;
			this.tooltip_text = "[E] PICK UP " + playerVision.item_type + ".";
		} else if (playerVision.action_avaiable) {
			this.tooltip_needed = true;
			this.tooltip_text = "[E] " + playerVision.action_text + "";
		} else {
			this.tooltip_needed = false;
		}
	}

	// If needed (and character not talking) will display tooltip text. (i.e: pick up axe, fix bridge, ...)
	private void showTooltipText() {
		if (tooltip_needed && !text_needed) {
			enable_display_text_small (tooltip_text);
		} else {
			disable_small_text ();
		}
	}

	// Controls ordered display of story bits on screen
	private void showStoryBits() {
		if (!showing_text && story_bits.Count > 0) {
			string next_text = story_bits [0];
			story_bits.RemoveAt (0);

			showing_text = true;
			current_text = next_text;
			waiting_for_dismiss = true;

			enable_display_text_large (current_text);

		} else if(showing_text) {

			// Check if time is up
			if(!waiting_for_dismiss) {
				showing_text = false;
				current_text = "";
				disable_large_text ();

				// Check fo see if there are more story bits to show
				text_needed = story_bits.Count > 0;
			}
		}
	}

	// Disable large text display
	private void disable_large_text() {
		text_large.GetComponent<CanvasGroup>().alpha = 0;
	}

	// Disable small text display
	private void disable_small_text() {
		text_small.GetComponent<CanvasGroup>().alpha = 0;
	}

	// Prints text in small display on screen
	private void enable_display_text_small(string text) {
		text_small.GetComponent<CanvasGroup>().alpha = 1;
		text_small_t.GetComponent<Text>().text = text;
	}

	// Prints text in large display on screen
	private void enable_display_text_large(string text) {
		text_large.GetComponent<CanvasGroup>().alpha = 1;
		text_large_t.GetComponent<Text>().text = text;
	}

	// Enables game feedback interface
	private void enable_display_text_feedback(string text) {
		feedback.GetComponent<CanvasGroup> ().alpha = 1;
		feedback_t.GetComponent<Text> ().text = text;
	}

	// Disables game feedback interface
	private void disable_display_text_feedback(string text) {
		feedback.GetComponent<CanvasGroup> ().alpha = 0;
		feedback_t.GetComponent<Text> ().text = "";
	}

}
