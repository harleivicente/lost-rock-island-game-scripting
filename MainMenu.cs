﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class MainMenu : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
		
	}
	
	// Update is called once per frame
	void Update () {

		// Exit game if pressing ESC
		if(Input.GetKeyDown(KeyCode.Escape)) {
			Application.Quit();
		}

		if(Input.GetKeyDown("space")) {
			SceneManager.LoadScene ("story_intro");
		}
	}
}
