Scripts for an 3d puzzle game developed using the Unity game engine. Completed game can be found here: http://gamejolt.com/games/lost-rock-island/137483. Aside from some nature assets I used the project was created from scratch as within 75 days as practice.

To see the game running simply download it from: http://gamejolt.com/games/lost-rock-island/137483 and play.

![image 1.png](https://bitbucket.org/repo/xaBXqo/images/2588171205-image%201.png)
![image 5.png](https://bitbucket.org/repo/xaBXqo/images/3524600813-image%205.png)